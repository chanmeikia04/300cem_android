package com.example.a300cem_android.QRCode;

import android.content.Context;
import android.content.Intent;

import com.example.a300cem_android.R;

import org.jetbrains.annotations.TestOnly;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCodePresenter {
    QRCodeDB dbData;
    QRCodePresenter.View view;
    ZXingScannerView scannerView;
    Context context;

    public QRCodePresenter(View view, ZXingScannerView scannerView, Context context) {
        this.view = view;
        this.scannerView = scannerView;
        this.context = context;
        dbData = new QRCodeDB(this);
    }

    @TestOnly
    public QRCodePresenter(QRCodeDB dbData, View view, ZXingScannerView scannerView, Context context) {
        this.dbData = dbData;
        this.view = view;
        this.scannerView = scannerView;
        this.context = context;
    }

    public void openQRCamera() {
        view.openQRCamera();
    }

    public void onRequestPermissionsResult(int requestCode, int grantResults) {
        if (requestCode == 100 && grantResults == 0){
            openQRCamera();
        }else{
            view.displayToastMessage(context.getString(R.string.Toast_noPermission));
        }
    }

    public void displayToastMessageOnView(String msg){
        view.displayToastMessage(msg);
    }

    public void getDataFromDB(String id) {
        dbData.getInfoData(id);
    }

    public void intent(Intent intent) {
        view.intent(intent);
    }

    public interface View {
        void displayToastMessage(String msg);
        void openQRCamera();
        void intent(Intent intent);
    }
}
