package com.example.a300cem_android.QRCode;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.a300cem_android.R;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCode extends AppCompatActivity implements QRCodePresenter.View, ZXingScannerView.ResultHandler{
    ZXingScannerView scannerView;
    QRCodePresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_r_code);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.mainPage_QRCode);

        scannerView = findViewById(R.id.ScannerView);
        presenter = new QRCodePresenter(this, scannerView, this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 100);
        }else{
            presenter.openQRCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        presenter.onRequestPermissionsResult(requestCode, grantResults[0]);
    }

    @Override
    protected void onStop() {
        scannerView.stopCamera();
        super.onStop();
    }

    public void openQRCamera() {
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void intent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void handleResult(Result rawResult) {
        presenter.getDataFromDB(rawResult.getText());
        presenter.openQRCamera();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

    @Override
    public void displayToastMessage(String msg) { Toast.makeText(this, msg, Toast.LENGTH_LONG).show(); }
}