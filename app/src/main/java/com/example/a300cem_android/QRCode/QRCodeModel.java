package com.example.a300cem_android.QRCode;

public class QRCodeModel {
    private String id, uuid, minor, major, website, pic_url;

    public QRCodeModel() {}

    public QRCodeModel(String id, String uuid, String minor, String major, String website, String pic_url) {
        this.id = id;
        this.uuid = uuid;
        this.minor = minor;
        this.major = major;
        this.website = website;
        this.pic_url = pic_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    @Override
    public String toString() {
        return "QRCodeModel{" +
                "id='" + id + '\'' +
                ", uuid='" + uuid + '\'' +
                ", minor='" + minor + '\'' +
                ", major='" + major + '\'' +
                ", website='" + website + '\'' +
                ", pic_url='" + pic_url + '\'' +
                '}';
    }
}
