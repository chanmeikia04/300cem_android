package com.example.a300cem_android.QRCode;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.a300cem_android.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class QRCodeDB {
    private QRCodePresenter presenter;
    private String id;

    public QRCodeDB() {
    }

    public QRCodeDB(QRCodePresenter presenter) {
        this.presenter = presenter;
    }

    public void getInfoData(String id) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://192.168.1.154:3000/products/"+id,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            getInfo(response);
                        } catch (JSONException e) {
                            presenter.displayToastMessageOnView(e.toString());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                presenter.displayToastMessageOnView(error.toString());
            }
        });
        Volley.newRequestQueue(presenter.context).add(jsonArrayRequest);
    }

    private void getInfo(JSONArray rs) throws JSONException {
        Log.d("DBTest", "Array: " + rs);
        if (rs.length() <= 0) {
            presenter.displayToastMessageOnView(presenter.context.getString(R.string.Toast_noRecord));
        } else {
            JSONObject o = rs.getJSONObject(0);

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(o.getString("website")));
            presenter.intent(browserIntent);
        }
    }
}
