package com.example.a300cem_android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.a300cem_android.Beacon.Beacon;
import com.example.a300cem_android.QRCode.QRCode;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.mainPage_title);

        ImageButton btnQRCode = findViewById(R.id.btnQRCode);
        btnQRCode.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.d("MainActivity", "QRCode");
                Intent intent = new Intent(MainActivity.this, QRCode.class);
                startActivity(intent);
            }
        });

        ImageButton btnBeacon = findViewById(R.id.btnBeacon);
        btnBeacon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.d("MainActivity", "Beacon");
                Intent intent = new Intent(MainActivity.this, Beacon.class);
                startActivity(intent);
            }
        });
    }
}