package com.example.a300cem_android.Beacon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.a300cem_android.QRCode.QRCodeModel;
import com.example.a300cem_android.R;

import java.util.ArrayList;

public class Beacon extends AppCompatActivity implements BeaconPresenter.View{
    protected static final String TAG = "Beacon-View";

    BeaconPresenter beaconPresenter;
    RecyclerView recycler_view;
    BeaconRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);
        setTitle(R.string.mainPage_Beacon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        beaconPresenter = new BeaconPresenter(this, this);

        recycler_view = findViewById(R.id.recycler_view_intro);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        checkPermission();
    }

    @Override
    public void checkPermission() {
        //BLE
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Location - GPS
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            beaconPresenter.start(bluetoothAdapter, gps, network);
        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
        }
    }

    @Override
    public void displayList(ArrayList<QRCodeModel> mData) {
        for(QRCodeModel m: mData){
            Log.i(TAG, m.toString());
        }
        adapter = new BeaconRecyclerViewAdapter(mData, beaconPresenter);
        recycler_view.setAdapter(adapter);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        beaconPresenter.onRequestPermissionsResult(requestCode, grantResults[0]);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

    @Override
    public void intent(Intent intent) { startActivity(intent); }

    @Override
    public void displayToastMessage(String msg) { Toast.makeText(this, msg, Toast.LENGTH_LONG).show(); }

}