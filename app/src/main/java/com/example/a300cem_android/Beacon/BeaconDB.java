package com.example.a300cem_android.Beacon;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.a300cem_android.QRCode.QRCodeModel;
import com.example.a300cem_android.R;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class BeaconDB implements BeaconConsumer {
    protected static final String TAG = "BeaconsModel";

    private BeaconPresenter beaconPresenter;

    private BeaconManager beaconManager;
    private ArrayList <QRCodeModel> mData = new ArrayList<QRCodeModel>();
    private int beaconNum = 0;


    public BeaconDB(BeaconPresenter presenter){
        this.beaconPresenter = presenter;
        mData = new ArrayList<>();
    }

    public void getBeaconList(){
        beaconManager = BeaconManager.getInstanceForApplication(beaconPresenter.context);
        beaconManager.setForegroundScanPeriod(1000L);
        beaconManager.setForegroundBetweenScanPeriod(10000L);
        beaconManager.setEnableScheduledScanJobs(true);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.bind(this);
        Log.i(TAG, "Scanning");
        beaconPresenter.displayToastMessageOnView(beaconPresenter.context.getString(R.string.Toast_Scanning));
    }

    public void onDestroy() {
        beaconManager.unbind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                mData.clear();

                beaconNum = beacons.size();

                if (beacons.size() > 0) {
                    for (Beacon beacon : beacons) {
                        getBeaconInfoData(beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString());
                    }
                }
            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {
            Log.i(TAG, e.toString());
        }
    }

    @Override
    public Context getApplicationContext() {
        return null;
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {}

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) { return false; }

    public void getBeaconInfoData(String uuid, String major, String minor) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://192.168.1.154:3000/products/" + uuid + "/" + minor + "/" + major,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            getInfo(response);
                        } catch (JSONException e) {
                            Log.e(TAG, "Error Msg：JSON");
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Msg：JSON - " + error.toString());
                beaconPresenter.displayToastMessageOnView(error.toString());
            }
        });
        Volley.newRequestQueue(beaconPresenter.context).add(jsonArrayRequest);
    }

    private void getInfo(JSONArray rs) throws JSONException {
        if (rs.length() > 0) {
                JSONObject o = rs.getJSONObject(0);

                QRCodeModel beaconsInfoDBModel = new QRCodeModel();
                beaconsInfoDBModel.setUuid(o.getString("uuid"));
                beaconsInfoDBModel.setMajor(o.getString("major"));
                beaconsInfoDBModel.setMinor(o.getString("minor"));


                beaconsInfoDBModel.setPic_url(o.getString("pic_url"));
                beaconsInfoDBModel.setWebsite(o.getString("website"));


                mData.add(beaconsInfoDBModel);
        }
        beaconNum--;
        if(beaconNum==0) {
            beaconPresenter.displayListToView(mData);
            onDestroy();
        }
    }
}
