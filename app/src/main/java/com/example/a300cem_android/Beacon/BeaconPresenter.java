package com.example.a300cem_android.Beacon;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;

import com.example.a300cem_android.QRCode.QRCodeModel;
import com.example.a300cem_android.R;

import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

public class BeaconPresenter {
    protected static final String TAG = "BeaconPresenter";

    BeaconPresenter.View view;
    BeaconDB model;
    Context context;

    public BeaconPresenter(View view, Context context) {
        this.view = view;
        this.context = context;
        model = new BeaconDB(this);
    }

    @TestOnly
    public BeaconPresenter(View view, BeaconDB model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
    }

    public void start(BluetoothAdapter bluetoothAdapter, boolean gps, boolean network) {
        if (bluetoothAdapter == null) {
            view.displayToastMessage(context.getString(R.string.doesnotSupportBluetooth));
        } else {
            //Check if GPS and Bluetooth is Turn on
            if ((gps || network) && bluetoothAdapter.isEnabled()) {
                model.getBeaconList();
            } else {
                displayToastMessageOnView("Please turn on Bluetooth and GPS");
            }
        }
    }

    @TestOnly
    public void start(String bluetoothAdapter, boolean gps, boolean network) {
        if (bluetoothAdapter == null) {
            view.displayToastMessage(context.getString(R.string.doesnotSupportBluetooth));
        } else {
            //Check if GPS and Bluetooth is Turn on
            if ((gps || network)) {
                model.getBeaconList();
            } else {
                displayToastMessageOnView("Please turn on Bluetooth and GPS");
            }
        }
    }

    public void intent(Intent intent) {
        view.intent(intent);
    }

    public void onRequestPermissionsResult(int requestCode, int grantResult) {
        if (requestCode == 100 && grantResult == 0){
            view.checkPermission();
        }else{
            view.displayToastMessage(context.getString(R.string.Toast_noPermission));
        }
    }

    public void displayToastMessageOnView(String msg){
        view.displayToastMessage(msg);
    }

    public void displayListToView(ArrayList<QRCodeModel> mData) {
        view.displayList(mData);
    }


    public interface View {
        void intent(Intent intent);
        void displayToastMessage(String msg);
        void checkPermission();
        void displayList(ArrayList<QRCodeModel> mData);
    }
}
