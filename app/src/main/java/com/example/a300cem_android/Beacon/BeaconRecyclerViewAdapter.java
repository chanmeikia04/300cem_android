package com.example.a300cem_android.Beacon;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.a300cem_android.QRCode.QRCodeModel;
import com.example.a300cem_android.R;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BeaconRecyclerViewAdapter extends RecyclerView.Adapter<BeaconRecyclerViewAdapter.ViewHolder> {
private static final String TAG = "RecyclerViewAdapter";
private static ArrayList<QRCodeModel> localDataSet;
private static BeaconPresenter presenter;

/**
 * Provide a reference to the type of views that you are using
 * (custom ViewHolder).
 */
public static class ViewHolder extends RecyclerView.ViewHolder {
    private Button btnLink;
    private ImageView ivPic;

    public ViewHolder(View view) {
        super(view);
        // Define click listener for the ViewHolder's View
        ivPic = view.findViewById(R.id.ivBeaconPic);
        btnLink = view.findViewById(R.id.btnLink);

        btnLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "OnClick ");
                //Intent to exhibition information
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(localDataSet.get(getAdapterPosition()).getWebsite()));
                presenter.intent(intent);
            }
        });

    }

    public ImageView getIvPic() { return ivPic; }
    public Button getBtnLink() { return btnLink; }
}

    public BeaconRecyclerViewAdapter(ArrayList<QRCodeModel> dataSet, BeaconPresenter presenter) {
        localDataSet = dataSet;
        this.presenter = presenter;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if(localDataSet.get(position).getPic_url()!="") {
            Glide.with(viewHolder.itemView.getContext()).load(localDataSet.get(position).getPic_url()).into(viewHolder.getIvPic());
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }
}
