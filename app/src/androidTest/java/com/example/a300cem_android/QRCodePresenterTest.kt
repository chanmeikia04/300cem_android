package com.example.a300cem_android

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.example.a300cem_android.QRCode.QRCodeDB
import com.example.a300cem_android.QRCode.QRCodeModel
import com.example.a300cem_android.QRCode.QRCodePresenter
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.junit.After
import org.junit.Before
import org.junit.Test

class QRCodePresenterTest {
    lateinit var qrCodePresenter: QRCodePresenter

    @RelaxedMockK
    lateinit var qrCodeModel: QRCodeModel

    @RelaxedMockK
    lateinit var qrCodeDB: QRCodeDB

    @RelaxedMockK
    lateinit var view: QRCodePresenter.View

    @RelaxedMockK
    lateinit var zXingScannerView: ZXingScannerView

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun SetUp(){
        MockKAnnotations.init(this)
        qrCodePresenter = QRCodePresenter(qrCodeDB, view, zXingScannerView, context)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun testDisplayToastMessageOnView() {
        // Given
        val mockData = "Toast Message"
        // When
        qrCodePresenter.displayToastMessageOnView(mockData)
        // Then
        verify {
            view.displayToastMessage(mockData)
        }
    }

    @Test
    fun testOpenQRCamera() {
        // Given

        // When
        qrCodePresenter.openQRCamera()
        // Then
        verify {
            view.openQRCamera()
        }
    }

    @Test
    fun testOnRequestPermissionsResult() {
        // Given
        val requestCode = 100
        val grantResults = 0

        // When
        qrCodePresenter.onRequestPermissionsResult(requestCode, grantResults)
        // Then
        verify {
            if (requestCode == 100 && grantResults == 0) {
                qrCodePresenter.openQRCamera()
            } else {
                view.displayToastMessage("No permission")
            }
        }
    }

    @Test
    fun testGetDataFromDB() {
        // Given
        val mockKData = "6098e10643f0450d317a5c18"
        // When
        qrCodePresenter.getDataFromDB(mockKData)
        // Then
        verify {
            qrCodeDB.getInfoData(mockKData)
        }
    }

    @Test
    fun testIntent() {
        // Given
        val mockData = Intent(Intent.ACTION_VIEW, Uri.parse("http://yahoo.com.hk"))

        // When
        qrCodePresenter.intent(mockData)

        // Then
        verify {
            view.intent(mockData)
        }
    }
}