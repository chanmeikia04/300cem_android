package com.example.a300cem_android

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.example.a300cem_android.Beacon.BeaconDB
import com.example.a300cem_android.Beacon.BeaconPresenter
import com.example.a300cem_android.QRCode.QRCodeModel
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class BeaconPresenterTest {
    lateinit var beaconPresenter: BeaconPresenter

    @RelaxedMockK
    lateinit var qrCodeModel: QRCodeModel

    @RelaxedMockK
    lateinit var beaconDB: BeaconDB

    @RelaxedMockK
    lateinit var view: BeaconPresenter.View

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun SetUp(){
        MockKAnnotations.init(this)
        beaconPresenter = BeaconPresenter(view, beaconDB, context)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun testDisplayToastMessageOnView() {
        // Given
        val mockData = "Toast Message"
        // When
        beaconPresenter.displayToastMessageOnView(mockData)
        // Then
        verify {
            view.displayToastMessage(mockData)
        }
    }


    @Test
    fun testOnRequestPermissionsResult() {
        // Given
        val requestCode = 100
        val grantResults = 0

        // When
        beaconPresenter.onRequestPermissionsResult(requestCode, grantResults)
        // Then
        verify {
            if (requestCode == 100 && grantResults == 0) {
                view.checkPermission()
            } else {
                view.displayToastMessage("No permission")
            }
        }
    }

    @Test
    fun testIntent() {
        // Given
        val mockData = Intent(Intent.ACTION_VIEW, Uri.parse("http://yahoo.com.hk"))

        // When
        beaconPresenter.intent(mockData)

        // Then
        verify {
            view.intent(mockData)
        }
    }

    @Test
    fun testStart() {
        // Given
        val bluetoothAdapter = "123"
        val gps = true
        val network = true

        // When
        beaconPresenter.start(bluetoothAdapter, gps, network)

        // Then
        verify {
            if (bluetoothAdapter == null) {
                view.displayToastMessage("This device doesn't support Bluetooth")
            } else {
                //Check if GPS and Bluetooth is Turn on
                if ((gps || network)) {
                    beaconDB.getBeaconList()
                } else {
                    view.displayToastMessage("Please turn on Bluetooth and GPS")
                }
            }
        }
    }
}